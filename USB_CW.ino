#include <Keyboard.h>
#include <Mouse.h>

#define CW 0
#define BUZZER 1
#define BUTTON 2
#define PLUG 3
#define WPM 20
#define KEYBOARD dvorak

// mouse_mode and buzzer_on are used in handle_cw and change_mode
int mouse_mode = 1;
int buzzer_on = 0;

// new_word is used in handle_cw and type
int new_word = 0;

// last_time is used in handle_cw and timepoint
unsigned long last_time = millis();

typedef struct morse_node {
  char value;
  morse_node *dit, *dah;
} morse_node;

typedef struct mapping {
  char from;
  char to;
} mapping;

// morse is used in morse_for and character_from (could probably wrap in a facade)
morse_node morse;

char dvorak(char qwerty) {
  static mapping dvorak[36] = {
    {'-','\''}, {'=',']'}, {'+','}'},
    {'q','x'}, {'w',','}, {'e','d'}, {'r','o'}, {'t','k'}, {'y','t'}, {'u','f'}, {'i','g'}, {'o','s'}, {'p','r'},
    {'a','a'}, {'s',';'}, {'d','h'}, {'f','y'}, {'g','u'}, {'h','j'}, {'j','c'}, {'k','v'}, {'l','p'}, {':','Z'}, {'\'','q'}, {'"','Q'},
    {'z','/'}, {'x','b'}, {'c','i'}, {'v','.'}, {'b','n'}, {'n','l'}, {'m','m'}, {',','w'}, {'.','e'}, {'/','['}, {'?','{'}
  };

  for(int i = 0; i < 36; i++) {
    if(dvorak[i].from == qwerty) { return dvorak[i].to; }
  }

  return qwerty;
}

char qwerty(char qwerty) {
  return qwerty;
}

void morse_for(char value, String sequence) {
  morse_node *current = &morse;
  for (int i = 0; i < sequence.length(); i++) {
    if (sequence.charAt(i) == '.') {
      if (current->dit == 0) { current->dit = calloc(1, sizeof(morse_node)); }
      current = current->dit;
    }
    if (sequence.charAt(i) == '-') {
      if (current->dah == 0) { current->dah = calloc(1, sizeof(morse_node)); }
      current = current->dah;
    }
  }
  if (current->value == 0) { current->value = value; }
}

char character_from(String sequence) {
  morse_node *current = &morse;
  for (int i = 0; i < sequence.length(); i++) {
    if (sequence.charAt(i) == '.') { current = current->dit; }
    if (sequence.charAt(i) == '-') { current = current->dah; }
    if (current == 0) { return 0; }
  }
  return current->value;
}

int timepoint() {
  unsigned long t = millis();
  int td = (int) (t - last_time);
  last_time = t;
  return td;
}

void type(char c) {
  static int word_length = 0;
  
  if(c == '\x15') { mistake(word_length); word_length = 0; return; }
  
  Keyboard.print(KEYBOARD(c));
  word_length++;
  
  if ((c == ' ') || (c == '\n')) { new_word = 1; word_length = 0; return; } // TODO 20191113 ap - if we push the word_length to a stack, then we can delete multiple words with HH
  if (new_word) { new_word = 0; }
}

void mistake(int length) {
  for (int i = 0; i < length; i++) { Keyboard.print('\x08'); }
}

void setup() {
  pinMode(CW,  INPUT_PULLUP);
  pinMode(PLUG,  INPUT_PULLUP);
  pinMode(BUTTON,  INPUT_PULLUP);
  pinMode(BUZZER,  OUTPUT);
  Serial.begin(9600);

  Serial.println("USB/CW started.");

  // Initialise
  Mouse.begin();
  Keyboard.begin();
  build_morse();
}

void build_morse() {
  morse_for('a', ".-");
  morse_for('b', "-...");
  morse_for('c', "-.-.");
  morse_for('d', "-..");
  morse_for('e', ".");
  morse_for('f', "..-.");
  morse_for('g', "--.");
  morse_for('h', "....");
  morse_for('i', "..");
  morse_for('j', ".---");
  morse_for('k', "-.-");
  morse_for('l', ".-..");
  morse_for('m', "--");
  morse_for('n', "-.");
  morse_for('o', "---");
  morse_for('p', ".--.");
  morse_for('q', "--.-");
  morse_for('r', ".-.");
  morse_for('s', "...");
  morse_for('t', "-");
  morse_for('u', "..-");
  morse_for('v', "...-");
  morse_for('w', ".--");
  morse_for('x', "-..-");
  morse_for('y', "-.--");
  morse_for('z', "--..");

  morse_for('0', "-----");
  morse_for('1', ".----");
  morse_for('2', "..---");
  morse_for('3', "...--");
  morse_for('4', "....-");
  morse_for('5', ".....");
  morse_for('6', "-....");
  morse_for('7', "--...");
  morse_for('8', "---..");
  morse_for('9', "----.");


  morse_for('&', ".-...");
  morse_for('\'', ".----.");
  morse_for('@', ".--.-.");
  morse_for(')', "-.--.-");
  morse_for('(', "-.--.");
  morse_for(':', "---...");
  morse_for(',', "--..--");
  morse_for('=', "-...-");
  morse_for('!', "-.-.--");
  morse_for('.', ".-.-.-");
  morse_for('-', "-....-");
  morse_for('+', ".-.-.");
  morse_for('"', ".-..-.");
  morse_for('?', "..--..");
  morse_for('/', "-..-.");

  morse_for('\n', ".-.-"); // <AA> - Newline
  morse_for('\x15', "........"); // <HH> - Mistake, redo word
}

void translate(String sequence) {
  Serial.print(sequence + " => ");
  char result = character_from(sequence);
  if (result != 0) {
    type(result);
    Serial.println(result);
  }
  else {
    Serial.println("Unknown");
  }
}

void loop() {
  handle_cw();
  delay(5); // debounce CW
  
  change_mode();
}

void activate_usb() {
  Serial.println("Key plugged in.");
  Mouse.begin();
  Keyboard.begin();
}

void deactivate_usb() {
  Serial.println("Unplugged.");
  Mouse.end();
  Keyboard.end();
}

void handle_cw() {
  static String sequence = "";
  static int plugged_in = 1;
  static int key_pressed = 0;

  static int dot_length = 1200 / WPM;
  static int word_space = dot_length * 10;
  static int letter_space = dot_length * 3;
  
  if ((!plugged_in) && (digitalRead(PLUG) == HIGH)) {
    activate_usb();
    plugged_in = 1;
  }

  if (!plugged_in) { return; }
  
  if (digitalRead(PLUG) == LOW) { deactivate_usb(); plugged_in = 0; return; }

  if (digitalRead(CW) == LOW) { // Key down
    if (key_pressed) { return; }

    digitalWrite(BUZZER, buzzer_on);
    key_pressed = 1;

    if (mouse_mode) { Keyboard.press(' '); return; }
    
    int gap = timepoint();
    if ((gap > word_space) && (!new_word)) {
      type(' ');
    }
  }
  else if (key_pressed) { // Key up
    digitalWrite(BUZZER, LOW);
    key_pressed = 0;

    if (mouse_mode) { Keyboard.release(' '); return; }
    
    int keyed = timepoint();
    if (keyed < (dot_length * 2)) {
      sequence += ".";
    }
    else {
      sequence += "-";
    }
  }
  
  // time out
  if ((sequence.length() > 0) && (!key_pressed) && ((millis() - last_time) > letter_space)) {
    translate(sequence);
    sequence = "";
  }
}

void change_mode() {
  static int mode_change = 0;
  
  if (digitalRead(BUTTON) == HIGH) { mode_change = 0; return; }
  if (mode_change) { return; }
  
  delay(30); // debounce
  if (digitalRead(BUTTON) == HIGH) { return; }
  
  mode_change = 1;
  if (!buzzer_on) { buzzer_on = 1; }
  else { mouse_mode = !mouse_mode; buzzer_on = 0; }

  digitalWrite(BUZZER, HIGH);
  delay(50);
  digitalWrite(BUZZER, LOW);

  Serial.println(String("Buzzer ") + (buzzer_on ? "on" : "off"));
  Serial.println(String("Key ") + (digitalRead(CW) == LOW ? "down" : "up"));
  Serial.println(String("Plug ") + (digitalRead(PLUG) == LOW ? "in" : "out"));
}
